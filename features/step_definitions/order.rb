Dado("esteja logado") do
  steps %{
    Quando o usuario entra com os dados
  }
end
  
Dado("pesquisa de um produto") do
  steps %{
    Quando o usuario pesquisa o produto 'Fusion Backpack'
  }
end
  
Quando("o usuario realiza o checkout") do
  @register.checkout
end
  
Quando("preenche os dados do checkout") do
  @register.register_order
end
  
Então("o usuario realiza o pedido com sucesso") do
  print 'Sucess' if expect(page).to have_text 'Thank you for your purchase!'
end