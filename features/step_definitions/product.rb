Quando("o usuario pesquisa o produto {string}") do |string|
  @products.add_products(string)
end
  
Então("é encontrado o produto {string}") do |string|
  @products.confirmation(string)
end