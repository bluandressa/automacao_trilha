Dado("que o usuário acessa o site Luma") do
  url = "https://magento.nublue.co.uk/"
  visit(url)
end

Dado("que o usuario esteja na pagina de cadastro") do
  click_link_or_button "Create an Account"
end

Quando("é preenchido os dados para criação de conta") do
  @customer.register_account
end
  
Então("é verificado se conta foi criada com sucesso") do
  puts 'Sucess' if expect(page).to have_text 'Thank you for registering with Main Website Store.'
end
