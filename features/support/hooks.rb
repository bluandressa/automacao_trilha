Before do
  page.driver.browser.manage.window.resize_to(1280, 720)

@register ||= Order.new 
@customer ||= Customer.new 
@products ||= Products.new 
@login    ||= Login.new 
end

After do |scenario|
  img = page.save_screenshot("data/screenshots/#{scenario.__id__}.png")
  embed(img, "image/png", "Evidência")
end


