require 'selenium-webdriver'
require 'capybara/cucumber'
require 'pry'
require 'faker'
require 'report_builder'
require 'rspec'

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.default_max_wait_time = 10
  end
Capybara.default_selector = :xpath

EL = YAML.load_file(File.join(Dir.pwd, "/data/elements.yml"))

