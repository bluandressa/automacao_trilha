class Order
    include Capybara::DSL
    
def register_order
    if page.has_css?('.shipping-address-item.selected-item')
        find(EL["input_tabela_fixa"]).click
        find(EL["botao_continue"]).click
    else
        find(EL["nome_rua"]).set Faker::Address.street_name
        find(EL["input_cidade"]).set Faker::Address.city 
        find(EL["option_estado"]).click
        find(EL["input_cep"]).set Faker::Address.postcode
        find(EL["input_telefone"]).set '11958888888'
        find(EL["input_pais"]).click
        find(EL["botao_continue"]).click
    end
    find(EL["botao_ordem"]).click()
end 

def checkout
    find(EL["botao_add_carrinho"]).click
    sleep 3
    find(EL["clicar_carrinho"]).click
    find(EL["botao_checkout"]).click
end

end