#language: pt

Funcionalidade: Cadastro 

  Contexto: Testar funcionalidade de cadastro
    Dado que o usuário acessa o site Luma

  @creat_customer
  Cenario: Criar conta para customer
    Dado que o usuario esteja na pagina de cadastro
    Quando é preenchido os dados para criação de conta
    Então é verificado se conta foi criada com sucesso 