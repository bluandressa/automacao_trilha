#language: pt

Funcionalidade: Realizar compra

  Contexto: Testar fluxo de compra
    Dado que o usuário acessa o site Luma
    E esteja logado


  @order
  Cenario: Textar fluxo de compra com sucesso
    Dado pesquisa de um produto 
    Quando o usuario realiza o checkout
    E preenche os dados do checkout
    Então o usuario realiza o pedido com sucesso