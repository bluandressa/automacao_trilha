#language: pt

Funcionalidade: Realizar buscar de produtos

  Contexto: 
    Dado que o usuário acessa o site Luma

  @product
  Cenario: Testar busca de produtos
    Quando o usuario pesquisa o produto 'Fusion Backpack'
    Então é encontrado o produto 'Fusion Backpack'